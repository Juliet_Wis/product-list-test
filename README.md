# Product list 
Test project

## Main functions:
* Add product
* Edit product
* Remove product
* Filter products by name
* Sort products by count or price

## Optional functions:
* Confirm, add and edit modals
* Highlighting invalid values
* Consider the current filter and sort when add or edit products