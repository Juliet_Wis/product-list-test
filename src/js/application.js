import $ from 'jquery'
import ProductTable from './element/productTable'
import ProductFilter from './element/productFilter'
import ProductService from "./service/productService";
import ModalAddOrEdit from './element/modalAddOrEdit';
import ModalConfirm from "./element/modalConfirm";

export default class Application {
    constructor (data) {
        this.productService = new ProductService(data, this.onUpdateHandler.bind(this));
        this.productTable = new ProductTable(this.onSortingChange.bind(this));
        this.productFilter = new ProductFilter(this.onFilterHandler.bind(this));
        this.modalAdd = new ModalAddOrEdit(this.onAddHandler.bind(this), this.onUpdateProductHandler.bind(this));
        this.modalConfirm = new ModalConfirm(this.onDeleteHandler.bind(this))
    }

    init () {
        $('#product-section').append(`
            <div class="row"><h1 class="mb-5">Products</h1></div>
            ${this.productFilter.draw()}
            ${this.productTable.draw()}
            ${this.modalConfirm.draw()}
        `);
        $('#filter-row').append(this.modalAdd.draw())

        this.update()

        this.productTable.bindHandlers();
        this.productFilter.bindHandlers();
        this.modalAdd.bindHandlers();
        this.modalConfirm.bindHandlers()
    }

    update () {
        let result = this.productService.getProducts(
            this.productFilter.trimFilterString(),
            this.productTable.sortingField,
            this.productTable.sortingOrder);
        this.productTable.update(result);
    }

    onFilterHandler () {
        this.update()
    }

    onUpdateHandler () {
        this.update()
    }

    onSortingChange () {
        this.update()
    }

    onAddHandler(product){
        this.productService.addProduct(product)
    }

    onDeleteHandler(productId){
        this.productService.removeProduct(productId)
    }

    onUpdateProductHandler(product){
        this.productService.updateProduct(product)
    }
}