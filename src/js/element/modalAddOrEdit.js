import $ from "jquery";
import Abstract from "./abstract";

export default class ModalAddOrEdit extends Abstract{
    constructor(onAdd, onUpdate){
        super();
        this.onUpdate = onUpdate;
        this.onAdd = onAdd;
        this.nameString = '';
        this.countNumber = Number('');
        this.priceNumber = Number('');
        this.isEditModal = false;
    }

    draw(){
        return `
            <div class="col-6 px-0 mb-2">
                <div class="btn-group float-right">
                    <button 
                      id="add-new-button"
                      type="button"
                      class="btn btn-primary" 
                      data-action="add"
                      data-toggle="modal" 
                      data-target="#modal-add"
                    >Add New</button>  
                </div>         
            </div>
            <div 
              class="modal fade" 
              id="modal-add" 
              tabindex="-1" 
              role="dialog" 
              aria-labelledby="exampleModalCenterTitle" 
              aria-hidden="true"
              data-backdrop="static">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content px-3 pb-3">
                  <div class="modal-body">
                  
                  <div class="row float-right">
                    <div class="col-8 px-2 mb-2">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  </div>
                                      
                  <div class="form-row">
                    <div class="col-8 px-2 mb-2">              
                      <label for="name-input">Name</label>
                        <input 
                        id="name-input" 
                        type="text" 
                        class="form-control" 
                        placeholder="Enter name"
                        required>
                        <div id="invalid-name-text" class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-3 pt-4 mb-2">              
                      <small>* Required field</small>
                    </div>
                  </div>
                  
                  <div class="form-row">
                    <div class="col-4 px-2 mb-2">              
                      <label for="count-input">Count</label>
                        <input 
                        id="count-input" 
                        type="text" 
                        class="form-control" 
                        placeholder="Enter count"
                        required>
                        <div id="invalid-count-text" class="invalid-feedback">
                        </div>
                    </div>
                  </div>
                  
                  <div class="form-row">
                    <div class="col-8 px-2 mb-2">              
                      <label for="price-input">Price</label>
                        <input 
                        id="price-input" 
                        type="text" 
                        class="form-control" 
                        placeholder="Enter price"
                        required>
                        <div id="invalid-price-text" class="invalid-feedback">
                        </div>
                    </div>
                  </div>
                  
                    <div class="form-row">
                      <div class="col-8 p-2 mb-2">
                        <button 
                          id="add-button"
                          type="button"
                          class="btn btn-secondary">
                         Add</button>                                                  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        `
    }

    bindHandlers() {
        this.bindAddButton();
        this.bindNameInput();
        this.bindCountInput();
        this.bindPriceInput();
        this.bindModalElement();
    }

    bindModalElement () {
        $('#modal-add')
            .on('show.bs.modal', e => {
                const button = $(e.relatedTarget);
                this.action = button.data('action');
                if (this.action === 'edit') {
                    this.product = {
                        'id': button.data('productId'),
                        'name': button.data('productName'),
                        'count': button.data('productCount'),
                        'price': button.data('productPrice')
                    }
                    this.initInputs()
                    this.isEditModal = true
                    $('#add-button').text('Update')
                } else if (this.action === 'add') {
                    this.isEditModal = false
                }
            })
            .on('hidden.bs.modal', () => {
                this.clearInputs();
                $('#add-button').text('Add')
                this.isEditModal = false
            })
    }

    bindAddButton(){
        $('#add-button').bind('click', () => {
            if (this.checkInputData()){
                if(this.isEditModal){
                    this.product.name = this.nameString
                    this.product.count = this.countNumber
                    this.product.price = this.priceNumber
                    this.onUpdate(this.product)
                }
                else{
                    this.product = {
                        'name': this.nameString,
                        'count': this.countNumber,
                        'price': this.priceNumber
                    }
                    this.onAdd(this.product)
                }
                this.clearInputs();
                $('#modal-add').modal('hide')
            }
        });
    }

    bindNameInput () {
        $('#name-input').bind('input', () => {
            this.nameString = String($('#name-input').val())
        });

        $('#name-input').bind('blur', () => {
            this.checkNameString()
        });
    }

    bindCountInput () {
        $('#count-input').bind('input', () => {
            $('#count-input').val($('#count-input').val().replace(/\D/g, ''))
            this.countNumber = Number($('#count-input').val())
        })
    }

    bindPriceInput () {
        $('#price-input').bind('focus', () => {
            $('#price-input').val(this.priceString)
        });

        $('#price-input').bind('input', () => {
            this.priceString = $('#price-input').val()
            this.priceNumber = Number(this.priceString)
        });

        $('#price-input').bind('blur', () => {
            this.checkPriceNumber();
        });
    }

    checkInputData(){
        this.checkNameString()
        return !($('#name-input').hasClass('is-invalid') ||
            $('#count-input').hasClass('is-invalid') ||
            $('#price-input').hasClass('is-invalid'))
    }

    checkNameString(){
        if (this.nameString === ''){
            $('#name-input').addClass('is-invalid')
            $('#invalid-name-text').text('Name can\'t be empty')
        }
        else if (this.nameString.trim() === ''){
            $('#name-input').addClass('is-invalid')
            $('#invalid-name-text').text('Name can\'t contain only spaces')
        }
        else if (this.nameString.length > 15){
            $('#name-input').addClass('is-invalid')
            $('#invalid-name-text').text('Name can\' contain more than 15 symbols')
        }
        else{
            $('#name-input').removeClass('is-invalid')
        }
    }

    checkPriceNumber(){
        if (isNaN(this.priceNumber)){
            $('#price-input').addClass('is-invalid')
            $('#invalid-price-text').text('Price must be a number')
        }
        else{
            this.priceUsd = new Intl.NumberFormat('en', {style: 'currency',
                currency: 'USD'})
                .format(this.priceNumber)
            $('#price-input').val(this.priceUsd)
            $('#price-input').removeClass('is-invalid')
        }
    }

    clearInputs(){
        $('#name-input').val('').removeClass('is-invalid')
        $('#count-input').val('')
        $('#price-input').val('').removeClass('is-invalid')
        this.nameString = '';
        this.countNumber = Number('')
        this.priceNumber = Number('')
        this.priceString = ''
    }

    initInputs(){
        this.nameString = String(this.product.name)
        this.countNumber = this.product.count
        this.priceNumber = this.product.price
        this.priceString = this.product.price
        this.priceUsd = new Intl.NumberFormat('en', {style: 'currency',
            currency: 'USD'})
            .format(this.priceNumber)
        $('#name-input').val(this.nameString)
        $('#count-input').val(this.countNumber)
        $('#price-input').val(this.priceUsd)
    }
}