import $ from "jquery";
import Abstract from "./abstract";

export default class ModalConfirm extends Abstract{
    constructor(onDelete){
        super()
        this.onDelete = onDelete
        this.productId = undefined
    }

    draw(){
        return `
            <div 
              class="modal fade" 
              id="modal-confirm" 
              tabindex="-1" 
              role="dialog" 
              aria-labelledby="modalConfirmTitle" 
              aria-hidden="true" 
              data-backdrop="static">>
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header justify-content-center">
                    <h5 class="modal-title" id="modalConfirmTitle">Are you sure?</h5>
                  </div>
                  <div class="modal-body text-center">
                  Are you sure you want to perform this action?
                  </div>
                  <div class="modal-footer justify-content-center">
                    <button 
                      id="yes-button" 
                      type="button" 
                      class="btn btn-primary" 
                      data-dismiss="modal"
                    >Yes</button>
                    <button 
                      type="button" 
                      class="btn btn-secondary" 
                      data-dismiss="modal"
                    >No</button>
                  </div>
                </div>
              </div>
            </div>
        `
    }

    bindHandlers() {
        this.bindDeleteButton();
        this.bindYesButton();
    }

    bindDeleteButton(){
        $('#modal-confirm').on('show.bs.modal',  e => {
            this.productId = $(e.relatedTarget).data('productId')
        })
    }

    bindYesButton(){
        $('#yes-button').bind('click', () => {
        if (this.productId !== undefined)
            this.onDelete(this.productId)
        })
    }
}